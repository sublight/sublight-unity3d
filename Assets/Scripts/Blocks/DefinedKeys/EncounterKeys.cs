﻿namespace LunraGames.SubLight
{
	public class EncounterKeys : KeyDefinitions
	{
		#region Booleans
		#endregion

		#region Integers
		#endregion

		#region Strings
		#endregion

		#region Floats
		#endregion

		public EncounterKeys() : base(KeyValueTargets.Encounter)
		{
			Booleans = new Boolean[]
			{

			};

			Integers = new Integer[]
			{

			};

			Strings = new String[]
			{

			};

			Floats = new Float[]
			{

			};

			Enumerations = new IEnumeration[]
			{

			};
		}
	}
}
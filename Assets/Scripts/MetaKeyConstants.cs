﻿namespace LunraGames.SubLight
{
	public static class MetaKeyConstants
	{
		public static class Values
		{
			public const string True = "true";
			public const string False = null;
		}

		public static class EncounterInfo
		{
			public const string EncounterId = "EncounterId";
		}

		public static class GalaxyInfo
		{
			public const string GalaxyId = "GalaxyId";
		}

		public static class GamemodeInfo
		{
			public const string GamemodeId = "GamemodeId";
		}

		public static class Game
		{
			public const string IsCompleted = "IsCompleted";
		}
	}
}
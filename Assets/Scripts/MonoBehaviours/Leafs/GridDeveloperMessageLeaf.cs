﻿using UnityEngine;

using TMPro;

namespace LunraGames.SubLight.Views
{
	public class GridDeveloperMessageLeaf : MonoBehaviour
	{
		public CanvasGroup Group;
		public RectTransform Canvas;
		public Transform CanvasAnchor;
		public TextMeshProUGUI MessageLabel;
		public XButton Button;
	}
}
